<?php
class ClienteModel extends CI_Model {
    public function tabela(){
        echo "<form action='Index.php?selec=I' name='cadastro' class='main-form' method='post'>
                    <h5 class='text-center'>Marca:
                    </h5>
                    <select name='marca' class='form-control'>
                            <option value='Chevrolet'>Chevrolet</option>
                            <option value='Volkswagen'>Volkswagen</option>
                            <option value='Fiat'>Fiat</option>
                            <option value='Ford'>Ford</option>
                    </select>
                    <h5 class='text-center'>Carroceria:
                        </h5>
                        <select name='carroceria' class='form-control'>
                        <option value='Hatch'>Hatch</option>
                                <option value='Sedan'>Sedan</option>
                                <option value='Perua'>Perua</option>
                                <option value='SUV'>SUV</option>
                    </select>
                    
                        <h5 class='text-center'>
                        <h5 class='text-center'>Cambio:
                        </h5>
                        <select name='cambio' class='form-control'>
                        <option value='Automático'>Automático</option>
                                <option value='Manual'>Manual</option>
                                <option value='Semi-Automático'>Semi-Automático</option>
                            
                    </select>
                    
                        <h5 class='text-center'>
                Modelo:<input class='form-control form-control-sm' type='text' name='modelo'  maxlength='240' value='' />
                    </h5>
                    <h5 class='text-center'>
            
                    <h5>Descrição:
                    <div class='md-form' >
                                    <textarea style='border-style: solid;
                                    border-width: 1px;'type='text' id='message' name='descricao' rows='8' class='form-control md-textarea'></textarea>
                                    
                                </div>
                    </h5>
                    </h5>
                    <h5 class='text-center'>
                    Kilometragem:<input class='form-control form-control-sm' type='number' name='kilometragem' size='50' maxlength='20' value='' />
                    </h5>
                    
                    <h5 class='text-center'>
                    Combustível:<input class='form-control form-control-sm' type='text' name='combustivel' size='50' maxlength='65' value='' />
                    </h5>
                    <h5 class='text-center'>
                    Foto: <input type='text' class='form-control form-control-sm' name='imagem' size='50' maxlength='50' value='Imagens/Automoveis/' />
                    </h5>
                    <h5 class='text-center'>
                    Final de Placa:<input type='text' class='form-control form-control-sm' name='finalp' size='10' maxlength='1' value='' />
                    </h5>
                    <h5 class='text-center'>
                Preço: <div class='input-group mb-3'>
                <div class='input-group-prepend'>
                <span class='input-group-text'>R$</span>
                </div>
                <input type='text' name='preco' class='form-control' >
                <div class='input-group-append'>
                <span class='input-group-text'>,00</span>
                </div>
            </div>
                    </h5>
                    <h5 class='text-center'><input type='submit' class='btn btn-default' value='Cadastrar' /></h5>
                    
                </form>
                </div>";
            }

            elseif ( $selec == "I") {     # INCLUIR
                if ( isset($_POST['marca']) == FALSE || 
                isset($_POST['carroceria']) == FALSE || 
                isset($_POST['cambio']) == FALSE || 
                isset($_POST['modelo']) == FALSE ||
                isset($_POST['descricao']) == FALSE || 
                isset($_POST['kilometragem']) == FALSE || 
                isset($_POST['combustivel']) == FALSE || 
                $_POST['imagem'] == 'Imagens/Automoveis' || 
                $_POST['finalp'] == 0 || is_numeric($_POST['finalp'] == FALSE ) || 
                $_POST['preco'] == 0 || is_numeric($_POST['preco'] == FALSE ) ){
                    echo "<p class='p-2 m-2 bg-warning text-white'>Preencha corretamente todas as informações!</p>";
                    
                }
                
                else {
                $campos = 'marca, carroceria, cambio, modelo, descricao, kilometragem, combustivel, imagem, finalp, preco';
                $valores = "'" . $_POST['marca']       . "','" .
                                $_POST['carroceria']      . "','" .
                                $_POST['cambio']      . "','" .
                                $_POST['modelo']      . "','" .
                                $_POST['descricao']        . "','" .
                                $_POST['kilometragem']    . "','" .
                                $_POST['combustivel']  . "','" .
                                $_POST['imagem']      . "','" .
                                $_POST['finalp']       . "','" .
                                $_POST['preco']      . "'";

                if ( funInsert("tb_automoveis", $campos, $valores) == TRUE )
                    echo "<p class='p-2 m-2 bg-info text-white'>Automóvel cadastrado com sucesso!</p>"; 
                else 
                    echo "<p class='p-2 m-2 bg-warning text-white'>Erro ao cadastrar automóvel!</p>"; 
                }
                echo "<p class='m-2'><input type='submit' value='Voltar' onclick='history.go(-1)' /></p><div class='row'>";
            }
        }
    }
?>