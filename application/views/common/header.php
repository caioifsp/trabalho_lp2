<!DOCTYPE html>
<html>
  <head>
  
  <img src="<?= base_url('assets/Imagens/ban.jpg')?>" height="200" width="100%"/>
  <link href="Imagens/fav.ico" rel="icon" type="image/x-icon" />
    <title>Automóveis</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link href="<?= base_url('assets/mdb/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/mdb/css/mdb.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/mdb/css/style.css') ?>" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <style>
    .main-form {
    width: 450px;
    margin: auto;
}
      #busca{
        margin-left:37%;
        text-align:center;
      }
      #dvcard{
        display:inline-block;
       
        text-align:center;
        margin-left:6%;
        margin-top:2%;
        
      }
      body{
        background-color: white;
      }
      #ses{
        background-color:white;
        margin-left:10%;
        margin-right:10%;
        
      }
      footer{
        width:100%;
        text-align:center;
        margin-bottom:0;
        
        margin-left:none;
        clear:both;
        background-color:#0B0B61;
       
        }
      #preco{
        font-style:bold;
        color:green;
        font-size:18px;
      }
      
      nav{
        background-color:#0B0B61;
      }
      #detalhes{
        background-color:#0B0B61;
      }
      #imgdetal{
          border-radius:15px;
          border-color: black;
          margin-left:1cm;
          margin-right:1cm;
          margin-top:1cm;
          text-align:left;
      }
      #titulocar{
        text-align:center;
        margin-right:0.5cm;
        margin-top:20px;
        margin-left:0.5cm;
        font-family:arial;
        font-size:40px;
        color:white;
        background-color:#0B0B61;
        border-radius:10px;
      }
      #preço{
          text-align:left;
          margin-left:25%;
          margin-right:25%;
          color:#228B22;
          font-size:50px;
          font-family:fantasy;
      }
      #detalheslist{
        margin-left:9cm;
          text-align:left;
          font-family:Arial;
          color:blue;
      }
      #titulodesc{
          margin-left:20%;
          text-align:center;
      }

      #colinfos{
          margin-top:1.5cm;
          margin-left:1.5cm;
          border-style:solid;
          border:3px;
          border-color:black;
          
      }
      pre{
          border-style:solid;
          border-radius:8px;
          padding:2cm;
          margin-left:20%;
          margin-top:1cm;
          border-collapse: separate;
      }
      section{
        padding:20px;
      }
      }

    </style>