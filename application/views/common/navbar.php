<nav class="navbar navbar-expand-lg navbar-dark ">
  
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item ">
        <a class="nav-link" href="Index.php?selec=H"><i class="fas fa-home"></i>Home </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Index.php?selec=C"><i class="fas fa-car"></i>Cadastrar</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Index.php?selec=L"><i class="fas fa-list-alt"></i>Listar</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Index.php?selec=B"><i class="fas fa-search"></i>Buscar</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Index.php?selec=A"><i class="fas fa-wrench"></i>Alterar</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Index.php?selec=E"><i class="far fa-trash-alt"></i>Excluir</a>
      </li>
      
      
    </ul>
  </div>
</nav>
</head>