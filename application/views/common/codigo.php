</head>
 
 <body>
 
   <div class="ROW container col-md-12">
     <h1 style="background-color:#0B0B61;" class="p-2 m-2 text-white text-center">Automóveis</h1>
     
        
  <?php
     include "DB.php";
     if ( isset($_GET['selec']) )
       $selec = $_GET['selec'];
     else
       $selec = "H";


     if ( $selec == "H") {       # HOME
       echo "<!--Section: Main info-->
       <section class='mt-5 wow fadeIn'>
 
         <!--Grid row-->
         <div class='row'>
 
           <!--Grid column-->
           <div class='col-md-6 mb-4'>
 
             <img src='Imagens/ferrari.jpg' class='img-fluid z-depth-1-half' alt=''>
 
           </div>
           <!--Grid column-->
 
           <!--Grid column-->
           <div class='col-md-6 mb-4'>
 
             <!-- Main heading -->
             <h3 class='h3 mb-3'>Anuncie Seu Automóvel</h3>
             <p>Venda mais rápido utilizando uma das melhores plataformas de compra e venda automóveis da WEB.</p>
             
             <!-- Main heading -->
 
             <hr>
 
             <p>
               E caso queira adquirir um automóvel, podemos te ajudar. Temos diversos modelos, semi-novos, usados e 0km!
             </p>
 
             
 
           </div>
           <!--Grid column-->
 
         </div>
         <!--Grid row-->
 
       </section>
       <!--Section: Main info--></div>";
       
     }

     elseif ( $selec == "C" ) {   # CADASTRAR
       echo "<form action='Index.php?selec=I' name='cadastro' class='main-form' method='post'>
             <h5 class='text-center'>Marca:
             </h5>
             <select name='marca' class='form-control'>
                   <option value='Chevrolet'>Chevrolet</option>
                   <option value='Volkswagen'>Volkswagen</option>
                   <option value='Fiat'>Fiat</option>
                   <option value='Ford'>Ford</option>
             </select>
             <h5 class='text-center'>Carroceria:
                 </h5>
                 <select name='carroceria' class='form-control'>
                 <option value='Hatch'>Hatch</option>
                       <option value='Sedan'>Sedan</option>
                       <option value='Perua'>Perua</option>
                       <option value='SUV'>SUV</option>
             </select>
              
               <h5 class='text-center'>
               <h5 class='text-center'>Cambio:
                 </h5>
                 <select name='cambio' class='form-control'>
                 <option value='Automático'>Automático</option>
                       <option value='Manual'>Manual</option>
                       <option value='Semi-Automático'>Semi-Automático</option>
                      
             </select>
              
               <h5 class='text-center'>
       Modelo:<input class='form-control form-control-sm' type='text' name='modelo'  maxlength='240' value='' />
             </h5>
             <h5 class='text-center'>
      
           <h5>Descrição:
             <div class='md-form' >
                           <textarea style='border-style: solid;
                           border-width: 1px;'type='text' id='message' name='descricao' rows='8' class='form-control md-textarea'></textarea>
                           
                       </div>
           </h5>
             </h5>
             <h5 class='text-center'>
             Kilometragem:<input class='form-control form-control-sm' type='number' name='kilometragem' size='50' maxlength='20' value='' />
             </h5>
            
             <h5 class='text-center'>
             Combustível:<input class='form-control form-control-sm' type='text' name='combustivel' size='50' maxlength='65' value='' />
             </h5>
             <h5 class='text-center'>
             Foto: <input type='text' class='form-control form-control-sm' name='imagem' size='50' maxlength='50' value='Imagens/Automoveis/' />
             </h5>
             <h5 class='text-center'>
             Final de Placa:<input type='text' class='form-control form-control-sm' name='finalp' size='10' maxlength='1' value='' />
             </h5>
             <h5 class='text-center'>
       Preço: <div class='input-group mb-3'>
       <div class='input-group-prepend'>
         <span class='input-group-text'>R$</span>
       </div>
       <input type='text' name='preco' class='form-control' >
       <div class='input-group-append'>
         <span class='input-group-text'>,00</span>
       </div>
     </div>
             </h5>
             <h5 class='text-center'><input type='submit' class='btn btn-default' value='Cadastrar' /></h5>
             
       </form>
       </div>";
     }

     elseif ( $selec == "I") {     # INCLUIR
       if ( isset($_POST['marca']) == FALSE || 
       isset($_POST['carroceria']) == FALSE || 
       isset($_POST['cambio']) == FALSE || 
       isset($_POST['modelo']) == FALSE ||
       isset($_POST['descricao']) == FALSE || 
       isset($_POST['kilometragem']) == FALSE || 
       isset($_POST['combustivel']) == FALSE || 
       $_POST['imagem'] == 'Imagens/Automoveis' || 
       $_POST['finalp'] == 0 || is_numeric($_POST['finalp'] == FALSE ) || 
       $_POST['preco'] == 0 || is_numeric($_POST['preco'] == FALSE ) ){
           echo "<p class='p-2 m-2 bg-warning text-white'>Preencha corretamente todas as informações!</p>";
           
       }
       
       else {
         $campos = 'marca, carroceria, cambio, modelo, descricao, kilometragem, combustivel, imagem, finalp, preco';
         $valores = "'" . $_POST['marca']       . "','" .
                          $_POST['carroceria']      . "','" .
                          $_POST['cambio']      . "','" .
                          $_POST['modelo']      . "','" .
                          $_POST['descricao']        . "','" .
                          $_POST['kilometragem']    . "','" .
                          $_POST['combustivel']  . "','" .
                          $_POST['imagem']      . "','" .
                          $_POST['finalp']       . "','" .
                          $_POST['preco']      . "'";

         if ( funInsert("tb_automoveis", $campos, $valores) == TRUE )
           echo "<p class='p-2 m-2 bg-info text-white'>Automóvel cadastrado com sucesso!</p>"; 
         else 
             echo "<p class='p-2 m-2 bg-warning text-white'>Erro ao cadastrar automóvel!</p>"; 
       }
       echo "<p class='m-2'><input type='submit' value='Voltar' onclick='history.go(-1)' /></p><div class='row'>";
     }
     elseif ( $selec == "L" ) {		#LISTAR
         $tabela = funSelect("tb_automoveis", "*", "");
         
             
             for ($i = 0; $i < count($tabela); $i++) {
           echo"
           <div id='dvcard' class='col-md-3 card'>
    
           <div class='view overlay'>
             <img class='card-img-top' src='". $tabela[$i]['IMAGEM'] . "' alt='Card image cap'>
             <a href='#!'>
               <div class='mask rgba-white-slight'></div>
             </a>
           </div>
         
           <div class='card-body'>
         
             <!-- Title -->
             <h4 class='card-title'>". $tabela[$i]['MODELO'] . "</h4>
             <!-- Text -->
             
             <p  id='preco'>R$". $tabela[$i]['PRECO'] . ",00</p>
             <!-- Button -->
             
             <a id='detalhes' href='Index.php?selec=D&cod=" .$tabela[$i]['CODIGO'] . "' class='btn text-center text-white'>Detalhes</a>
         
           </div></div>";
         
         
         }  
    
         
         echo "</div></div>";
         
         
     }
     elseif ( $selec == "D" ) {		# DETALHAR
         $cod = $_GET['cod'];
         $argumentos = "WHERE CODIGO = '$cod' ";
         $tabela = funSelect("tb_automoveis", "*", $argumentos);
         echo "
                 
          
          <div class='col-md-12'>
     <h2 id='titulocar'>". $tabela[0]['MODELO'] . "</h2>
   </div>
   <div class='row col-md-12'>
     <div class='col-md-5'>
     <img id='imgdetal' src='". $tabela[0]['IMAGEM'] . "' width='500' height='250'>
   </div>
   <div class='col-md-6 sm-12' id='colinfos'>
     <h2 id='preço' width='500' height='250'>R$". $tabela[0]['PRECO'] .",00</h2>
       <div class='row'>
       <table class='table'>
 <thead>
   <tr>
     <th scope='col'><i class='far fa-registered'></i> Marca</th>
     <th scope='col'><i class='fas fa-car-side'></i> Carroceria</th>
     <th scope='col'><i class='fas fa-sitemap'</i> Cambio</th>
     <th scope='col'><i class='fas fa-road'></i> Kilometragem</th>
     <th scope='col'><i class='fas fa-gas-pump'></i> Combustível</th>
     
     
   </tr>
 </thead>
 <tbody>
   <tr>
     <td>". $tabela[0]['MARCA'] . "</td>
     <td>". $tabela[0]['CARROCERIA'] . "</td>
     <td>". $tabela[0]['CAMBIO'] . "</td>
     <td>". $tabela[0]['KILOMETRAGEM'] . "KM</td>
     <td>". $tabela[0]['COMBUSTIVEL'] . "</td>
    
   </tr>
   
 </tbody>
</table>
       </div>
   </div>

 
 
       
 </div>
 <hr>
   
   <div class='col-md-10'>
       <h2 id='titulodesc'>DESCRIÇÃO:</h2>
       <pre>". $tabela[0]['DESCRICAO'] . "</pre>
   </div>
   <div class='col-md-2'></div>
          
          
          ";
/*
        echo "<div class='row'>
        <div class='col text-center font-weight-bold'>Quartos: </div>
        <div class='col'>" . $tabela[0]['QUARTOS'] . "</div>
        <div class='col text-center font-weight-bold'>Banheiros: </div>
        <div class='col'>" . $tabela[0]['BANHEIROS'] . "</div>
        <div class='col text-center font-weight-bold'>Vagas: </div>
        <div class='col'>" . $tabela[0]['VAGAS'] . "</div>
        <div class='col text-center font-weight-bold'>Valor: </div>
  <div class='col'>R$ " . number_format($tabela[0]['VALOR'],2,',','.') . "</div>
        </div>
    </div>
    <br />
    <p class='m-2'><input type='submit' value='Voltar' onclick='history.go(-1)' /></p>";*/
     }
     elseif ( $selec == "B" ) {		# BUSCAR
       echo "<div id='busca' class='col-md-3'><form action='Index.php?selec=R' method='post'>
               Marca:
                <select name='marca' class='form-control'>
                   <option value='Chevrolet'>Chevrolet</option>
                   <option value='Volkswagen'>Volkswagen</option>
                   <option value='Fiat'>Fiat</option>
                   <option value='Ford'>Ford</option>
             </select>
             Cambio:
             <select name='cambio' class='form-control'>
             <option value='Automático'>Automático</option>
                   <option value='Manual'>Manual</option>
                   <option value='Semi-Automático'>Semi-Automático</option>
                  
         </select>
                        
                 <h5 class='text-center'>
             Combustível:<input class='form-control form-control-sm' type='text' name='combustivel' size='50' maxlength='65'  />
             </h5> 
                 <h5 class='text-center'><input type='submit' class='btn btn-default' value='Buscar' /></h5>

           </form>
           </div></div>";
     }
    
         elseif ( $selec == "R" ) { 		# RESULTADO DA BUSCA
           if ( isset($_POST['marca']) == FALSE || isset($_POST['cambio']) == FALSE 
                 || isset($_POST['combustivel']) == FALSE ) {
                   echo "<p class='p-2 m-2 bg-warning text-white'>Preencha todas as informações!</p>";
               }
               else {
             $argumentos = " WHERE MARCA = '" . $_POST['marca'] . "' 
                       AND CAMBIO = '" . $_POST['cambio'] . "'
                       AND COMBUSTIVEL = '" . $_POST['combustivel'] . "'";
       
             $tabela = funSelect("tb_automoveis", "*", $argumentos);
                 echo "<h5 class='p-2 m-2 bg-secondary text-white text-center'>Resultado da Busca - "
                 . $_POST['marca'] . " - " . $_POST['cambio']  . " - " .$_POST['combustivel']." </h5>";
                 
                 for ($i = 0; $i < count($tabela); $i++) {
                   echo"
           <div id='dvcard' class='col-md-3 card'>
    
           <div class='view overlay'>
             <img class='card-img-top' src='". $tabela[$i]['IMAGEM'] . "' alt='Card image cap'>
             <a href='#!'>
               <div class='mask rgba-white-slight'></div>
             </a>
           </div>
         
           <div class='card-body'>
         
             <!-- Title -->
             <h4 class='card-title'>". $tabela[$i]['MODELO'] . "</h4>
             <!-- Text -->
             
             <p  id='preco'>R$". $tabela[$i]['PRECO'] . ",00</p>
             <!-- Button -->
             
             <a id='detalhes' href='Index.php?selec=D&cod=" .$tabela[$i]['CODIGO'] . "' class='btn text-center text-white'>Detalhes</a>
         
           </div></div>";
                 }  
                 echo "</div>";
               }
             }
             elseif ( $selec == "A" ) { 	
               $tabela = funSelect("tb_automoveis", "*", "");
               for ($i = 0; $i < count($tabela); $i++) {
                 echo"
         <div id='dvcard' class='col-md-3 card'>
  
         <div class='view overlay'>
           <img class='card-img-top' src='". $tabela[$i]['IMAGEM'] . "' alt='Card image cap'>
           <a href='#!'>
             <div class='mask rgba-white-slight'></div>
           </a>
         </div>
       
         <div class='card-body'>
       
           <!-- Title -->
           <h4 class='card-title'>". $tabela[$i]['MODELO'] . "</h4>
           <!-- Text -->
           
           <p  id='preco'>R$". $tabela[$i]['PRECO'] . ",00</p>
           <!-- Button -->
           
           <a id='detalhes' href='Index.php?selec=M&cod=" .$tabela[$i]['CODIGO'] . "' class='btn text-center text-white'>Alterar  <i class='fas fa-wrench'></i></a>
           
         </div></div>";
               }  
               echo"</div>";
             }
         elseif ( $selec == "M" ) {   # MODIFICAR
           $cod = $_GET['cod'];
           $argumentos = " WHERE CODIGO = '$cod' ";
           $tabela = funSelect("tb_automoveis", "*", $argumentos);
           echo "<form action='Index.php?selec=U&cod=$cod' name='cadastro' class='main-form' method='post'>
           <h5 class='text-center'>Marca:
           </h5>
           <select name='marca' class='form-control'>
                 <option value='". $tabela[0]['MARCA']." 'selected>". $tabela[0]['MARCA']."</option>
                 <option value='Chevrolet'>Chevrolet</option>
                 <option value='Volkswagen'>Volkswagen</option>
                 <option value='Fiat'>Fiat</option>
                 <option value='Ford'>Ford</option>
           </select>
           <h5 class='text-center'>Carroceria:
               </h5>
               <select name='carroceria' class='form-control'>
               <option value='". $tabela[0]['CARROCERIA']." 'selected>". $tabela[0]['CARROCERIA']."</option>
               <option value='Hatch'>Hatch</option>
                     <option value='Sedan'>Sedan</option>
                     <option value='Perua'>Perua</option>
                     <option value='SUV'>SUV</option>
           </select>
            
             <h5 class='text-center'>
             <h5 class='text-center'>Cambio:
               </h5>
               <select name='cambio' class='form-control'>
               <option value='". $tabela[0]['CAMBIO']." 'selected>". $tabela[0]['CAMBIO']."</option>
               <option value='Automático'>Automático</option>
                     <option value='Manual'>Manual</option>
                     <option value='Semi-Automático'>Semi-Automático</option>
                    
           </select>
            
             <h5 class='text-center'>
     Modelo:<input class='form-control form-control-sm' type='text' name='modelo'  maxlength='240' value='". $tabela[0]['MODELO']."' />
           </h5>
           <h5 class='text-center'>
    
         <h5>Descrição:
           <div class='md-form' >
                         <textarea style='border-style: solid;
                         border-width: 1px;'type='text' id='message' value='". $tabela[0]['DESCRICAO']."' name='descricao' rows='8' class='form-control md-textarea'>". $tabela[0]['DESCRICAO']."</textarea>
                         
                     </div>
         </h5>
           </h5>
           <h5 class='text-center'>
           Kilometragem:<input class='form-control form-control-sm' type='number' name='kilometragem' size='50' maxlength='20' value='". $tabela[0]['KILOMETRAGEM']."' />
           </h5>
          
           <h5 class='text-center'>
           Combustível:<input class='form-control form-control-sm' type='text' name='combustivel' size='50' maxlength='65' value='". $tabela[0]['COMBUSTIVEL']."' />
           </h5>
           <h5 class='text-center'>
           Foto: <input type='text' class='form-control form-control-sm' name='imagem' size='50' maxlength='50' value='". $tabela[0]['IMAGEM']."' />
           </h5>
           <h5 class='text-center'>
           Final de Placa:<input type='text' class='form-control form-control-sm' name='finalp' size='10' maxlength='1' value='". $tabela[0]['FINALP']."' />
           </h5>
           <h5 class='text-center'>
     Preço: <div class='input-group mb-3'>
     <div class='input-group-prepend'>
       <span class='input-group-text'>R$</span>
     </div>
     <input type='text' name='preco' class='form-control' value='". $tabela[0]['PRECO']."'>
     <div class='input-group-append'>
       <span class='input-group-text'>,00</span>
     </div>
   </div>
           </h5>
           <h5 class='text-center'><input type='submit' class='btn btn-default' value='Cadastrar' /></h5>
           
     </form>
          ";
         
         }
         elseif ( $selec == "U") {     # COMANDO UPDATE
           $cod = $_GET['cod'];
           if ( isset($_POST['marca']) == FALSE || 
           isset($_POST['carroceria']) == FALSE || 
           isset($_POST['cambio']) == FALSE || 
           isset($_POST['modelo']) == FALSE ||
           isset($_POST['descricao']) == FALSE || 
           isset($_POST['kilometragem']) == FALSE || 
           isset($_POST['combustivel']) == FALSE || 
           $_POST['imagem'] == 'Imagens/Automoveis' || 
           $_POST['finalp'] == 0 || is_numeric($_POST['finalp'] == FALSE ) || 
           $_POST['preco'] == 0 || is_numeric($_POST['preco'] == FALSE ) ){
               echo "<div><p class='p-2 m-2 bg-warning text-white'>Preencha corretamente todas as informações!</p></div>";
           }
           else {
             
             $alteracoes = "MARCA='" . $_POST['marca']       . "'," .
                           "CARROCERIA='"   .$_POST['carroceria']      . "'," .
                           "CAMBIO='"   .$_POST['cambio']        . "'," .
                           "MODELO='"   .$_POST['modelo']    . "'," .
                           "DESCRICAO='"   .$_POST['descricao']  . "'," .
                           "KILOMETRAGEM='"  . $_POST['kilometragem']      . "'," .
                           "COMBUSTIVEL='"   .$_POST['combustivel']       . "'," .
                           "IMAGEM='"   .$_POST['imagem']       . "'," .
                           "FINALP='"   .$_POST['finalp']       . "'," .
                           "PRECO='"   .$_POST['preco']      . "'";
             $argumentos = "WHERE CODIGO ='$cod' ";
   
             if ( funUpdate("tb_automoveis", $alteracoes, $argumentos) == TRUE )
               echo "<p class='p-2 m-2 bg-info text-white'>Automóvel alterado com sucesso!</p>"; 
             else 
               echo "<p class='p-2 m-2 bg-info text-white'>Automóvel alterado com sucesso!</p>"; 
              
           }
           echo "<p class='m-2'><input type='submit' value='Voltar' onclick='history.go(-1)' /></p>";
         }
         elseif ( $selec == "E" ) {		#EXCLUIR
           $tabela = funSelect("tb_automoveis", "*", "");
             for ($i = 0; $i < count($tabela); $i++) {
               echo"
               <div id='dvcard' class='col-md-3 card'>
        
               <div class='view overlay'>
                 <img class='card-img-top' src='". $tabela[$i]['IMAGEM'] . "' alt='Card image cap'>
                 <a href='#!'>
                   <div class='mask rgba-white-slight'></div>
                 </a>
               </div>
             
               <div class='card-body'>
             
                 <h4 class='card-title'>". $tabela[$i]['MODELO'] . "</h4>
             
                 
                 <p  id='preco'>R$". $tabela[$i]['PRECO'] . ",00</p>
               
                 <a id='detalhes' href='' class='btn text-center text-white' data-toggle='modal' data-target='#modalConfirmDelete'>Excluir</a>
                 <!--Modal: modalConfirmDelete-->
                 <div class='modal fade' id='modalConfirmDelete' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel'
                   aria-hidden='true'>
                   <div class='modal-dialog modal-sm modal-notify modal-danger' role='document'>
                     <!--Content-->
                     <div class='modal-content text-center'>
                       <!--Header-->
                       <div class='modal-header d-flex justify-content-center'>
                         <p class='heading'>Confirma a exclusão do automóvel atual?</p>
                       </div>
               
                       <div class='modal-body'>
                 
                         <i class='fas fa-times fa-4x animated rotateIn'></i>
                 
                       </div>
                 
                   
                       <div class='modal-footer flex-center'>
                         <a href='Index.php?selec=X&cod=" .$tabela[$i]['CODIGO'] . "' class='btn  btn-outline-danger'>Sim</a>
                         <a type='button' class='btn  btn-danger waves-effect' data-dismiss='modal'>Não</a>
                       </div>
                     </div>
                    
                   </div>
                 </div>
               
                   </div>
               </div>";
                     }  
                     echo "</div>";
             echo "</div>";
         }
         elseif ( $selec == "X") {     # COMANDO UPDATE
           $cod = $_GET['cod'];
          
             $argumentos = "WHERE CODIGO ='$cod' ";
   
             if ( funDelete("tb_automoveis",  $argumentos) == TRUE )
               echo "<p class='p-2 m-2 bg-info text-white'>Automóvel excluído com sucesso!</p>"; 
             else 
               echo "<p class='p-2 m-2 bg-warning text-white'>Erro ao excluir Automóvel!</p>"; 

           echo "<p class='m-2'><input type='submit' value='Voltar' onclick='history.go(-1)' /></p>";
         }
 

  ?>
